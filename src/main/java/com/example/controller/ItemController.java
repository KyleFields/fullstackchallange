package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.model.Item;
import com.example.repository.ItemDao;

@Controller
@RequestMapping("/api")
//@CrossOrigin(origins="http://localhost:4200")
@CrossOrigin(origins="*") //cors - cross origin resource sharing
public class ItemController {
	
	private ItemDao ItemDao;
	
	public ItemController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ItemController(ItemDao ItemDao) {
		super();
		this.ItemDao = ItemDao;
	}
	
//	@RequestMapping(value="/getAllItems.app", method=RequestMethod.GET)
	@GetMapping(value="/getAllItems.app")
	public @ResponseBody List<Item> getAllItems() {
//		insertInitialValues();
		System.out.println("in getAllItems");
		return ItemDao.selectAll();
	}
	
	@PostMapping(value="/getItemById.app", produces="application/json", params= {"id"})
	//the form field needs the same name as the java variable name,
	//another attribute is the "consumes" and its counterpart "produces"
	//"comsumes" defines the format of the request body
	//"produces" defines the format of the response body.
	public ResponseEntity<Item> getItemById(int id) {
		return new ResponseEntity<Item>(ItemDao.selectById(id), HttpStatus.I_AM_A_TEAPOT);
	}
	
	//effectively deas the same as above just without the custom http status
	@PostMapping(value="/getItemById2.app")
	public @ResponseBody Item getItemById2(@RequestParam("id") int ItemId) {
		return ItemDao.selectById(ItemId);
	}
	
 	@PostMapping(value="/getItemById3.app", consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Item getItemById3(@RequestBody Integer id) {
		return ItemDao.selectById(id);
	}
	
	@PostMapping(value="/{num}/getItemByUri.app")
	public @ResponseBody Item getItemByUri(@PathVariable("num") int num) {
		return ItemDao.selectById(num);
	}
	
	public void insertInitialValues() {
		Item Item1 = new Item("Pho", 400.98);
		Item Item2 = new Item("Ramen", 380.12);
		Item Item3 = new Item("Sushi Roll", 120.60);
		Item Item4 = new Item("Pizza", 500.00);
		
		ItemDao.insert(Item1);
		ItemDao.insert(Item2);
		ItemDao.insert(Item3);
		ItemDao.insert(Item4);
	}
	
	

}
