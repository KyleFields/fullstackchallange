package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller("weDontneedAnameForourBeanifWeDontwant")
public class ViewController {
	
	@PostMapping("/next.app")
	public String getPage() {
		//forward
//		return "nextpage.html";
		return "age";
		
		//REDIRECT
//		return "redirect: https://www.reddit.com/";
		
	}

}
