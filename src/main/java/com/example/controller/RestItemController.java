package com.example.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Item;
import com.example.repository.ItemDao;

/*
 * What is a RESTful API?
 * 	An API that follows the REST Constraints
 * 
 * @RestController vs @Controller StereoTypes
 * 		@RestController assumes you're a RESTful api and automatically uses
 * 		@ResponseBody on your return types.
 * 
 * What are the Rest constraints?
 * 		1. Uniform Interface
 * 			in short, each entity's api should follow the same pattern. No entity
 * 			should have more than one uri.
 * 
 * 		2. Client Server
 * 			The client and server are separate entities. They exist independently.
 * 			(Client should only knwo the URI, API is encapsulated.)
 * 
 * 		3. Stateless
 * 			Servers do not store any data about the lastest HTTP request. It treats
 * 			each request as though it is a new request. NO SESSIONS
 * 
 * 		4.	Cacheable
 * 			resources must declare themselves as cacheable
 * 
 * 		5. Layered System
 * 			Client cannot tell whether it's connected to the original server or
 * 			some other intermediate server.
 * 
 * 		6. Code on Demand (optional)
 * 			you're able to send functional code if necessary. (UI widget)
 */
@RestController
@RequestMapping("/restapi/Item")
public class RestItemController {
	
	private ItemDao ItemDao;
	
	public RestItemController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public RestItemController(ItemDao ItemDao) {
		super();
		this.ItemDao = ItemDao;
	}
	
	
	@GetMapping("")
	public List<Item> getAllItem() {
		return ItemDao.selectAll();
	}

}
