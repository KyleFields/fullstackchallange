package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Item")
public class Item {
	
	@Id
	@Column(name="code")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String code;
	
	@Column(name="name", unique=true, nullable=false)
	private String name;
	
	@Column(name="price", nullable=false)
	private double price;
	
	public Item() {
		// TODO Auto-generated constructor stub
	}

	public Item(String code, String name, double price) {
		super();
		this.code = code;
		this.name = name;
		this.price = price;
	}
	
	public Item(String name, double price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getcode() {
		return code;
	}

	public void setcode(String code) {
		this.code = code;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public double getprice() {
		return price;
	}

	public void setprice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Item [code=" + code + ", name=" + name + ", price=" + price + "]";
	}

}
