package com.example.repository;

import java.util.List;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Item;

@Transactional
@Repository("ItemDao")
public class ItemDao {
	
	private SessionFactory sesFact;
	
	public ItemDao() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ItemDao(SessionFactory sesFact) {
		super();
		this.sesFact = sesFact;
	}
	
	//@Transactional
	//if @Transactional is not put above the dao class, it is needed above each of the Dao methods
	public void insert(Item Item) {
		/*
		 * Session ses = sesFact.openSession();
		 * Transaction tx = ses.beginTransaction();
		 * 
		 * ses.save(Item);
		 * 
		 * tx.commit();
		 * ses.close();
		 */
		
		sesFact.getCurrentSession().save(Item);
	}
	
	public Item selectById(int id) {
		return sesFact.getCurrentSession().get(Item.class, id);
	}
	
	public List<Item> selectAll() {
		return sesFact.getCurrentSession().createQuery("from Item", Item.class).list();
	}

}
